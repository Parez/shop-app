import {Directive, Input, HostListener, ElementRef} from '@angular/core';

@Directive({
  selector: '[appSticky]'
})
export class StickyDirective {

  private useVH:boolean = false;
  private vhPercent:number = 0;

  private calc:number = 0;

  @Input() set moveY(val:number)
  {
    this.calc = val;
    this.positionElement();
  }


  _realOffsetY: number = 0;
  @Input() set offsetY(val:string){
    if(val.indexOf("vh") > -1)
    {
      this.useVH = true;
      this.updateDimensions();
      this.vhPercent = parseFloat(val)/100;
      this._realOffsetY = this.vhPercent*this.viewHeight;
    }
    else
    {
      this.updateDimensions();
      this._realOffsetY = parseFloat(val);
      console.log(this._realOffsetY);
      this.useVH = false;
    }

    this.positionElement();
  }


  element:HTMLElement;

  private viewWidth:number = 0;
  private viewHeight:number = 0;

  constructor(private elemRef: ElementRef) {
    this.element = this.elemRef.nativeElement;
    this.updateDimensions();
    this.positionElement();
  }

  updateDimensions()
  {
    if(!this.useVH) return;
    this.viewWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
    this.viewHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if(!this.useVH) return;
    this.updateDimensions();
    this.positionElement();
  }

  @HostListener('window:scroll', ['$event'])
  onScroll(event) {
    this.positionElement();
  }

  positionElement()
  {
    this._realOffsetY = this.vhPercent*this.viewHeight;
    let scroll = window.pageYOffset;
    if(scroll > this._realOffsetY+this.calc)
    {
      this.element.style.position = "fixed";
      this.element.style.top = "0";
      this.element.style.zIndex = "900";
    }
    else {
      this.element.style.position = "absolute";
      this.element.style.top = String(this._realOffsetY+this.calc)+"px";
      this.element.style.zIndex = "900";
    }
  }
}
