import {Item} from './Item';
/**
 * Created by baunov on 26/05/2017.
 */
export interface ICartItem
{
    item:Item;
    count:number;
}