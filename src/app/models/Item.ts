/**
 * Created by baunov on 25/05/2017.
 */
import * as shortid from "shortid";

export interface IItem
{
    id:string;
    name:string;
    price:number;
    description:string;
    cover:string; //cover image url
    images:string[]; //all images urls
    videoUrl:string;//URL for the intro video on the item page
    headerUrl:string;
}

export class Item implements IItem
{
    id:string;
    name:string = "Hand Spinner";
    price:number = 200;
    description:string = "Стильный спиннер, который можно использовать как для снятия стресса, так и просто для того чтобы убить врямя";
    cover:string = "./assets/test.png"; //cover image url
    images:string[]; //all images urls
    videoUrl:string = "https://www.w3schools.com/html/mov_bbb.mp4";//URL for the intro video on the item page
    headerUrl:string = "https://player.vimeo.com/external/158148793.hd.mp4?s=8e8741dbee251d5c35a759718d4b0976fbf38b6f&profile_id=119&oauth2_token_id=57447761";

    constructor(id = shortid.generate())
    {
        this.id = id;
    }

    public asObject():IItem
    {
        return {
            id: this.id,
            name: this.name,
            price: this.price,
            description: this.description,
            cover: this.cover,
            images: this.images,
            videoUrl: this.videoUrl,
            headerUrl: this.headerUrl
        }
    }
}