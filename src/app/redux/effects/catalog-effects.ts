import {Injectable} from '@angular/core';
import { Action } from '@ngrx/store';
import { Effect, Actions } from '@ngrx/effects';
import {Observable} from 'rxjs';
import * as cart from "../actions/cart-actions";
import {ItemsService} from '../../services/items.service';
/**
 * Created by baunov on 04/06/2017.
 */

@Injectable()
export class CatalogEffects {
    constructor(private actions$: Actions, private itemsService:ItemsService){}

    @Effect()
    addItem$: Observable<Action> = this.actions$
        .ofType(cart.ADD_ITEM)
        .do(console.log)
        .map((action) => action.payload)
        .switchMap( (id) => {
            return this.itemsService.getItemById(id).map((item) => {
                return new cart.AddItemSuccessAction(item);
            });
        });

    @Effect()
    removeItem$: Observable<Action> = this.actions$
        .ofType(cart.REMOVE_ITEM)
        .do(console.log)
        .map((action) => action.payload)
        .switchMap( (id) => {
            return Observable.of(new cart.RemoveItemSuccessAction(id));
        });
}