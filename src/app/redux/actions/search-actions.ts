import { Action } from '@ngrx/store';
import { Item } from '../../models/Item';

export const SEARCH =                 '[Search] Search';
export const SEARCH_SUCCESS =         '[Search] Search Success';
export const SEARCH_FAIL =            '[Search] Search Fail';

/**
 * Load Collection Actions
 */
export class SearchAction implements Action {
  readonly type = SEARCH;

  //some search parameters
  //this should be handled via elasticsearch
  constructor(public payload:any) {}
}

export class SearchSuccessAction implements Action {
  readonly type = SEARCH_SUCCESS;

  //all items returned by a search
  constructor(public payload: Item[]) { }
}

export class SearchFailAction implements Action {
  readonly type = SEARCH_FAIL;

  constructor(public payload: any) { }
}


export type Actions
  = SearchAction
  | SearchSuccessAction
  | SearchFailAction;
