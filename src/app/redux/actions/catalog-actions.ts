import { Action } from '@ngrx/store';
import { Item } from '../../models/Item';


export const SELECT_ITEM =          '[Catalog] Add Item';
export const LOAD =                 '[Catalog] Load';
export const LOAD_SUCCESS =         '[Catalog] Load Success';
export const LOAD_FAIL =            '[Catalog] Load Fail';


/**
 * Add Item to Collection Actions
 */
export class SelectItemAction implements Action {
  readonly type = SELECT_ITEM;

  constructor(public payload:string){}
}

/**
 * Load Collection Actions
 */
export class LoadAction implements Action {
  readonly type = LOAD;
}

export class LoadSuccessAction implements Action {
  readonly type = LOAD_SUCCESS;

  constructor(public payload: Item[]) { }
}

export class LoadFailAction implements Action {
  readonly type = LOAD_FAIL;

  constructor(public payload: any) { }
}


export type Actions
  = SelectItemAction
  | LoadAction
  | LoadSuccessAction
  | LoadFailAction;
