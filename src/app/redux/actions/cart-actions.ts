import { Action } from '@ngrx/store';
import { Item } from '../../models/Item';


export const ADD_ITEM =             '[Cart] Add Item';
export const ADD_ITEM_SUCCESS =     '[Cart] Add Item Success';
export const ADD_ITEM_FAIL =        '[Cart] Add Item Fail';

export const SET_ITEM_COUNT =       '[Cart] Set Item Count';
export const SET_ITEM_COUNT_SUCCESS =     '[Cart] Set Item Count Success';
export const SET_ITEM_COUNT_FAIL =        '[Cart] Set Item Count Fail';

export const REMOVE_ITEM =          '[Cart] Remove Item';
export const REMOVE_ITEM_SUCCESS =  '[Cart] Remove Item Success';
export const REMOVE_ITEM_FAIL =     '[Cart] Remove Item Fail';
export const LOAD =                 '[Cart] Load';
export const LOAD_SUCCESS =         '[Cart] Load Success';
export const LOAD_FAIL =            '[Cart] Load Fail';


/**
 * Add Item to Collection Actions
 */
export class AddItemAction implements Action {
  readonly type = ADD_ITEM;

  constructor(public payload: string) { }
}

export class AddItemSuccessAction implements Action {
  readonly type = ADD_ITEM_SUCCESS;

  constructor(public payload: Item) { }
}

export class AddItemFailAction implements Action {
  readonly type = ADD_ITEM_FAIL;

  constructor(public payload: Error) { }
}


/**
 * Remove Item from Collection Actions
 */
export class RemoveItemAction implements Action {
  readonly type = REMOVE_ITEM;

  constructor(public payload: string) { }
}

export class RemoveItemSuccessAction implements Action {
  readonly type = REMOVE_ITEM_SUCCESS;

  constructor(public payload: string) { }
}

export class RemoveItemFailAction implements Action {
  readonly type = REMOVE_ITEM_FAIL;

  constructor(public payload: Item) {}
}

/**
 * Load Collection Actions
 */
export class LoadAction implements Action {
  readonly type = LOAD;
}

export class LoadSuccessAction implements Action {
  readonly type = LOAD_SUCCESS;

  constructor(public payload: Item[]) { }
}

export class LoadFailAction implements Action {
  readonly type = LOAD_FAIL;

  constructor(public payload: any) { }
}


/*
 * Set Item Count Actions
 */

export class SetItemCountAction implements Action {
  readonly type = SET_ITEM_COUNT;


  constructor(public payload:{id:string, count:number}) {}
}

export class SetItemCountSuccessAction implements Action {
  readonly type = SET_ITEM_COUNT_SUCCESS;

  constructor(public payload: Item[]) { }
}

export class SetItemCountFailAction implements Action {
  readonly type = SET_ITEM_COUNT_FAIL;

  constructor(public payload: any) {}
}

export type Actions
  = AddItemAction
  | AddItemSuccessAction
  | AddItemFailAction
  | RemoveItemAction
  | RemoveItemSuccessAction
  | RemoveItemFailAction
  | LoadAction
  | LoadSuccessAction
  | LoadFailAction
    | SetItemCountAction
    | SetItemCountSuccessAction
    | SetItemCountFailAction;
