import * as search from '../actions/search-actions';
import {Set} from 'immutable';

export interface State {
    ids:Set<string>;
    loading: boolean;
    query: any;
}

const initialState: State = {
    ids:Set<string>(),
    loading: false,
    query: {}
};

export function reducer(state = initialState, action: search.Actions): State {
    switch (action.type) {
        case search.SEARCH: {
            const query = action.payload;

            //empty result for empty query
            if (Object.keys(query).length == 0) {
                return {
                    ids: Set<string>(),
                    loading: false,
                    query
                };
            }

            //otherwise set loading to true and wait for query
            //to trigger search_success action
            return Object.assign({}, state, {
                query,
                loading: true
            });
        }

        case search.SEARCH_SUCCESS: {
            const items = action.payload;

            return {
                ids: Set.of(...items.map(item => item.id)),
                loading: false,
                query: state.query
            };
        }

        default: {
            return state;
        }
    }
}


export const getIds = (state: State) => state.ids;

export const getQuery = (state: State) => state.query;

export const getLoading = (state: State) => state.loading;
