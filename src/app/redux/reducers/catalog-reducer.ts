import * as cart from '../actions/cart-actions';
import * as catalog from '../actions/catalog-actions';
import {Item, IItem} from '../../models/Item';
import {Action} from '@ngrx/store';
import {Set, Map} from 'immutable';
import {createSelector} from 'reselect';

export interface State {
    items:Map<string, IItem>;
    ids:Set<string>;
    selectedId:string;
}

export const initialState:State = {
    items: Map<string, IItem>(),
    ids:Set<string>(),
    selectedId: null
};

export function reducer(state = initialState, action:Action): State {
    console.log("Catalog", state, action);
    switch (action.type) {
        //when cart id is added to cart,
        // also add item to the cached catalog
        case cart.ADD_ITEM_SUCCESS:
        {
            const item:Item = action.payload.asObject();

            console.log(JSON.stringify(item));

            return Object.assign({}, state, {
                items: state.items.set(item.id, item),
                ids: state.ids.add(item.id)
            })
        }
        case catalog.SELECT_ITEM:
        {
            const itemId:string = action.payload;

            return Object.assign({}, state, {
                selectedId: itemId
            })
        }
    }
    return state;
}

//those should take "catalog" piece of the state as argument
export const getIds = (state: State) => state.ids.toArray();
export const getItems = (state: State) => state.items.toObject();
export const getSelectedId = (state: State) => state.selectedId;

export const getSelected = createSelector(getItems, getSelectedId, (items, selectedId) => {
    return items[selectedId];
});

//returns array from {id: item} object
export const getAll = createSelector(getItems, getIds, (items, ids) => {
    return ids.map(id => items[id]);
});
