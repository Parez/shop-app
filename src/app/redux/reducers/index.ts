import {createSelector} from 'reselect';
import {ActionReducer, combineReducers} from '@ngrx/store';
import * as fromCart from "./cart-reducer";
import * as fromCatalog from "./catalog-reducer";
import {ICartItem} from '../../models/ICartItem';
import {LocalStorageService} from '../../services/local-storage.service';
import {compose} from '@ngrx/core/compose';

const PROD:boolean = true;

const reducers = {
  cart: fromCart.reducer,
  catalog: fromCatalog.reducer
};

/*
Local Storage Manager
 */
// add a middleware for the compose

export const localStorageSync = (reducer: any) => {
  return function (state, action: any) {
    if (action.type === '@ngrx/store/init') {
      // only for the init action we update the state. Here we can call our function :)
      console.log("INIT from Local Storage");
      state = Object.assign({}, state, LocalStorageService.getSavedState());
    }

    return reducer(state, action);
  }
};


export function reducer(state: any = initialState, action: any) {
    return compose(localStorageSync, combineReducers)(reducers)(state, action);
}



export interface State {
  cart: fromCart.State;
  catalog: fromCatalog.State;
}

const initialState:State = {
  cart: fromCart.initialState,
  catalog: fromCatalog.initialState
};
//export const reducer: ActionReducer<State> = combineReducers(reducers);



export const getCatalogState = (state:State) => state.catalog;

export const getCatalogIds = createSelector(getCatalogState, fromCatalog.getIds);
export const getCatalogItems = createSelector(getCatalogState, fromCatalog.getItems);
export const getCatalogSelected = createSelector(getCatalogState, fromCatalog.getSelected);

export const getCartState = (state:State) => state.cart;

export const getCartIds = createSelector(getCartState, fromCart.getIds);
export const getCartCounts = createSelector(getCartState, fromCart.getCounts);
export const getCartTotalCount = createSelector(getCartState, (state:fromCart.State) => {
  return state.counts.toArray().reduce((total, cur) => total+cur, 0);
});


export const getCartItems = createSelector(getCatalogItems, getCartCounts, getCartIds, (items, counts, ids) => {
  return ids.map(id => {
    return {item: items[id], count: counts[id]};
  });
});

export const getCartTotalValue = createSelector(getCartItems, (items:ICartItem[]) => {
  return items.reduce((total:number, item:ICartItem) => {
    return total + item.item.price*item.count;
  }, 0);
});
