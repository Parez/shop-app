import * as cart from '../actions/cart-actions';
import {Item} from '../../models/Item';
import {Action} from '@ngrx/store';
import {Set, Map} from 'immutable';


export interface State {
    loaded: boolean;
    loading: boolean;
    ids: Set<string>,
    counts: Map<string, number>
}

export const initialState:State = {
    loaded:false,
    loading:false,
    ids: Set<string>(),
    counts: Map<string, number>()
};

export function reducer(state = initialState, action:Action): State {
    console.log("Cart", state, action);
    switch (action.type) {
        case cart.LOAD: {
            return Object.assign({}, state, {
                loading: true
            });
        }

        //first need to make sure this item is loaded locally
        case cart.ADD_ITEM_SUCCESS: {
            const itemId = action.payload.id;
            return addItem(state, itemId);
        }
        case cart.REMOVE_ITEM_SUCCESS: {
            const itemId = action.payload;
            return removeItem(state, itemId);
        }
        case cart.SET_ITEM_COUNT: {
            const itemId = action.payload.id;
            const count = action.payload.count;
            return setItemCount(state, itemId, count);
        }
    }
    return state;
}


const addItem = (state, itemId) => {
    //if there is at least one item of this kind in the cart
    //leave ids untouched, add 1 to the count of the item with that id
    if(state.ids.has(itemId)) {
        return Object.assign({}, state, {
            counts: state.counts.set(itemId, state.counts.get(itemId)+1)
        });
    }
    else {
        return Object.assign({}, state, {
            ids: state.ids.add(itemId),
            counts: state.counts.set(itemId, 1)
        });
    }
};

const removeItem = (state, itemId) => {
    if(state.ids.has(itemId)) {

        //if count of item is 1 or less
        //remove item from counts, remove item from ids
        if(state.counts.get(itemId) <= 1) {
            return Object.assign({}, state, {
                counts: state.counts.remove(itemId),
                ids: state.ids.remove(itemId)
            })
        }
        else { //else decrease the count of item
            return Object.assign({}, state, {
                counts: state.counts.set(itemId, state.counts.get(itemId)-1)
            });
        }
    }
    else {
        //if no such item, leave as is
        return state
    }
};

const setItemCount = (state, itemId, count) => {
    if(state.ids.has(itemId)) {
        if(count > 0)
        {
            return Object.assign({}, state, {
                counts: state.counts.set(itemId, count)
            });
        }
        else {
            return Object.assign({}, state, {
                counts: state.counts.remove(itemId),
                ids: state.ids.remove(itemId)
            })
        }
    }
    else {
        if(count > 0) {
            return Object.assign({}, state, {
                ids: state.ids.add(itemId),
                counts: state.counts.set(itemId, count)
            });
        }
    }
};

export const getIds = (state: State) => state.ids.toArray();
export const getCounts = (state: State) => state.counts.toObject();