import * as cart from '../actions/cart-actions';
import {Item, IItem} from '../../models/Item';
import {Action} from '@ngrx/store';
import {Set, Map} from 'immutable';
import {createSelector} from 'reselect';
import * as search from '../actions/search-actions';

export interface State {
    items:Map<string, IItem>;
    ids:Set<string>;
}

export const initialState:State = {
    items: Map<string, IItem>(),
    ids:Set<string>(),
};


//Local Database of items
//Both cart and catalog should take items from here
export function reducer(state = initialState, action:Action): State {
    console.log("ItemsReducer", state, action);
    switch (action.type) {
        //when cart id is added to cart,
        // also add item to the cached catalog
        case cart.ADD_ITEM_SUCCESS:
        {
            const item:Item = action.payload.asObject();

            console.log(JSON.stringify(item));

            return Object.assign({}, state, {
                items: state.items.set(item.id, item),
                ids: state.ids.add(item.id)
            })
        }
        //add items to local items database once search is done
        case search.SEARCH_SUCCESS:
        {
            const items:IItem[] = action.payload;

            return Object.assign({}, state, {
                items: state.items.merge(items.reduce((obj, item) => {
                    obj[item.id] = item;
                    return obj;
                }, {})),
                ids: state.ids.merge(items.map(item => item.id))
            })
        }

        //whe
    }
    return state;
}

//those should take "catalog" piece of the state as argument
export const getIds = (state: State) => state.ids.toArray();
export const getItems = (state: State) => state.items.toObject();


//returns array from {id: item} object
export const getAll = createSelector(getItems, getIds, (items, ids) => {
    return ids.map(id => items[id]);
});
