import {Component, OnInit, EventEmitter, Output} from '@angular/core';
import {FormGroup, Validators, FormBuilder} from '@angular/forms';
import {EMAIL} from '../../utils/RegexPatterns';

@Component({
  selector: 'app-checkout-form',
  templateUrl: './checkout-form.component.html',
  styleUrls: ['./checkout-form.component.scss']
})
export class CheckoutFormComponent implements OnInit {

  form: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.form = this.fb.group({
      email: ["", Validators.compose([Validators.required, Validators.pattern(EMAIL)])],
      phone: ["", Validators.required]
    });

  }
}
