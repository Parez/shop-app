import {Component, OnInit, Input} from '@angular/core';
import {Item} from '../../models/Item';

@Component({
  selector: 'app-row-item-list',
  templateUrl: './row-item-list.component.html',
  styleUrls: ['./row-item-list.component.scss']
})
export class RowItemListComponent implements OnInit {

  @Input() items:Item[] = [];
  constructor() { }

  ngOnInit() {
  }

}
