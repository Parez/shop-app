import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RowItemListComponent } from './row-item-list.component';

describe('RowItemListComponent', () => {
  let component: RowItemListComponent;
  let fixture: ComponentFixture<RowItemListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RowItemListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RowItemListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
