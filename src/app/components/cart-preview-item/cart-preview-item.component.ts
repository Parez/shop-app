import {Component, OnInit, Input} from '@angular/core';
import {Item} from '../../models/Item';
import {ICartItem} from '../../models/ICartItem';

@Component({
  selector: 'app-cart-preview-item',
  templateUrl: './cart-preview-item.component.html',
  styleUrls: ['./cart-preview-item.component.scss']
})
export class CartPreviewItemComponent implements OnInit {

  @Input() item:ICartItem;
  constructor() { }

  ngOnInit() {
  }

}
