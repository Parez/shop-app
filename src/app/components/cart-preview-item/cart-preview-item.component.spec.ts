import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CartPreviewItemComponent } from './cart-preview-item.component';

describe('CartPreviewItemComponent', () => {
  let component: CartPreviewItemComponent;
  let fixture: ComponentFixture<CartPreviewItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CartPreviewItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CartPreviewItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
