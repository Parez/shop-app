import {Component, OnInit, Input} from '@angular/core';
import {Item} from '../../models/Item';

@Component({
  selector: 'app-row-item',
  templateUrl: './row-item.component.html',
  styleUrls: ['./row-item.component.scss']
})
export class RowItemComponent implements OnInit {

  @Input() item:Item = new Item();
  constructor() { }

  ngOnInit() {
  }

}
