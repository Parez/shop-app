import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {Store} from '@ngrx/store';
import * as redux from "../../redux/reducers";
import {ICartItem} from '../../models/ICartItem';

@Component({
  selector: 'app-cart-icon',
  templateUrl: 'cart-icon.component.html',
  styleUrls: ['cart-icon.component.scss']
})
export class CartIconComponent implements OnInit {

  isOver:boolean = false;
  count$:Observable<number>;
  totalPrice$:Observable<number>;
  items$:Observable<ICartItem[]>;
  constructor(private store: Store<redux.State>) {}

  ngOnInit() {
    this.count$ = this.store.select(redux.getCartTotalCount);
    this.totalPrice$ = this.store.select(redux.getCartTotalValue);
  }

  onOver()
  {
    this.isOver = true;
  }

  onOut()
  {
    this.isOver = false;
  }

}
