import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {IItem} from '../../../models/Item';
import {FormGroup, FormBuilder} from '@angular/forms';

@Component({
  selector: 'app-add-item-form',
  templateUrl: './add-item-form.component.html',
  styleUrls: ['./add-item-form.component.scss']
})
export class AddItemFormComponent implements OnInit {


  @Output() submitted:EventEmitter<IItem> = new EventEmitter<IItem>();
  emailError:string;
  passwordError:string;
  form: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    //TODO: replace all forms with JSON representations
  }

}
