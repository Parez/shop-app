import {Component, OnInit, Input} from '@angular/core';
import {Observable} from 'rxjs';
import {Item} from '../../models/Item';
import {Store} from '@ngrx/store';
import * as redux from "../../redux/reducers";
import * as cart from "../../redux/actions/cart-actions";
import {ICartItem} from '../../models/ICartItem';

@Component({
  selector: 'app-item-count',
  templateUrl: './item-count.component.html',
  styleUrls: ['./item-count.component.scss']
})
export class ItemCountComponent implements OnInit {

  @Input() item:Item;

  count$:Observable<number>;
  constructor(private store:Store<redux.State>) {

  }

  addOne()
  {
    this.store.dispatch(new cart.AddItemAction(this.item.id));
  }

  removeOne()
  {
    this.store.dispatch(new cart.RemoveItemAction(this.item.id));
  }

  setCount(event)
  {
    let val = parseInt(event.target.value);
    if(!val) val = 0;

    if(val < 0)
    {
      val = 0;
    }
    this.store.dispatch(new cart.SetItemCountAction({id:this.item.id, count:val}));
  }

  ngOnInit() {
    this.count$ = this.store.select(redux.getCartItems)
        .map((items:ICartItem[]) => {
          let filtered = items.filter((item) => item.item.id === this.item.id);
          if(filtered.length > 0) return filtered[0].count;
          return 0;
        });
  }
}
