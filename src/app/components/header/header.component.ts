import {Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Input() height:string = "900px";
  @Input() imageUrl:string = "";
  @Input() videoUrl:string = "";
  constructor() { }

  ngOnInit() {
  }

}
