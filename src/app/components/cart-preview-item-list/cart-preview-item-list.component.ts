import {Component, OnInit, Input} from '@angular/core';
import {ICartItem} from '../../models/ICartItem';

@Component({
  selector: 'app-cart-preview-item-list',
  templateUrl: './cart-preview-item-list.component.html',
  styleUrls: ['./cart-preview-item-list.component.scss']
})
export class CartPreviewItemListComponent implements OnInit {

  @Input() items:ICartItem[];
  constructor() { }

  ngOnInit() {
  }

}
