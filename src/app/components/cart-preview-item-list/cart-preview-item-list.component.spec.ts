import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CartPreviewItemListComponent } from './cart-preview-item-list.component';

describe('CartPreviewItemListComponent', () => {
  let component: CartPreviewItemListComponent;
  let fixture: ComponentFixture<CartPreviewItemListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CartPreviewItemListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CartPreviewItemListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
