import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {ICartItem} from '../../models/ICartItem';
import {Store} from '@ngrx/store';
import * as redux from "../../redux/reducers";

@Component({
  selector: 'app-cart-preview',
  templateUrl: './cart-preview.component.html',
  styleUrls: ['./cart-preview.component.scss']
})
export class CartPreviewComponent implements OnInit {

  items$:Observable<ICartItem[]>;
  constructor(private store: Store<redux.State>) {
    this.items$ = store.select(redux.getCartItems);
  }

  ngOnInit() {

  }

}
