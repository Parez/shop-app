import {Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.scss']
})
export class VideoPlayerComponent implements OnInit {

  @Input() src = "http://static.videogular.com/assets/videos/videogular.mp4";
  constructor() { }

  ngOnInit() {
  }

}
