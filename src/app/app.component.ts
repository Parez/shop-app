import {Component, HostListener} from '@angular/core';
import {ItemsService} from './services/items.service';
import {Item} from './models/Item';
import {Observable} from 'rxjs';
import {LocalStorageService} from './services/local-storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {


  constructor(private storageService:LocalStorageService)
  {
  }


}
