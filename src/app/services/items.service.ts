import { Injectable } from '@angular/core';
import {Observable, BehaviorSubject} from 'rxjs';
import {Item} from '../models/Item';

@Injectable()
export class ItemsService {

  private cache:{[key:string]:Item} = {};
  private items:Item[] = [];

  private loadedItemsSubj:BehaviorSubject<Item[]> = new BehaviorSubject([]);
  public loadedItems$:Observable<Item[]> = this.loadedItemsSubj.asObservable();

  private loadedItems:Item[] = [];

  private lastId:string = "";
  private isLoading:boolean = false;
  private isEnd:boolean = false;

  constructor() {
    for(let i = 0; i < 100; i++)
    {
      let item = new Item(String(i));
      item.price = Math.round(i*30);
      this.items.push(item);
    }
  }

  /*public getItems(startWithId:string, count:number):Observable<Item[]>
  {
  }*/

  public loadNext(num:number)
  {
    if(this.isEnd) return;
    if(this.isLoading) return;
    this.isLoading = true;


    let sub = this.loadItems(this.lastId, num+1)
        .catch(e => Observable.of(`Error loading data: ${e}`)).subscribe( (items:Item[]) => {
      if(this.lastId == items[items.length-1].id)
      {
        this.isEnd = true;
      }
      else {
        this.loadedItems = this.loadedItems.concat(items.slice(0,num));
        this.lastId = items[items.length-1].id;
        this.loadedItemsSubj.next(this.loadedItems);
      }
      this.isLoading = false;
      sub.unsubscribe();
    });
  }

  private loadItems(startWithId:string = "", count:number = 10):Observable<Item[]>
  {
    console.log("Loading...");
    let startItemIndex = this.items.indexOf(this.items.filter(item => item.id == startWithId)[0]);
    if(startWithId == "") startItemIndex = 0;
    if(startItemIndex > -1)
    {
      return Observable.of(this.items.slice(startItemIndex, Math.min(startItemIndex+count,this.items.length)))
          .delay(1000);
    }

  }

  public getAllItems():Observable<Item[]>
  {
    return Observable.of(this.items);
  }

  public getItemByIdFromCache(uid:string):Item
  {
    return this.items.filter(item => item.id === uid)[0];
  }

  public getItemById(uid:string):Observable<Item> {
    if(this.cache[uid]) return Observable.of(this.cache[uid]);
    return Observable.of(this.items.filter(item => item.id === uid)[0]).do((item) => {
      this.cache[item.id] = item;
    });
  }


}
