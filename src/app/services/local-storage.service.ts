import { Injectable } from '@angular/core';
import * as redux from "../redux/reducers/";
import {Store} from '@ngrx/store';
import * as transit from 'transit-immutable-js';
import * as fromCart from "../redux/reducers/cart-reducer";
import * as fromCatalog from "../redux/reducers/catalog-reducer";

@Injectable()
export class LocalStorageService {

  constructor(private store:Store<redux.State>) {
    this.store.subscribe((state) => {
      console.log(state);

      //console.log(transit.toJSON(state.cart));

      window.localStorage.setItem("cart", transit.toJSON(state.cart));
      window.localStorage.setItem("catalog", transit.toJSON(state.catalog));

    });
  }

  public static getSavedState():redux.State
  {
    let state = {
      cart: transit.fromJSON(window.localStorage.getItem("cart")),
      catalog: transit.fromJSON(window.localStorage.getItem("catalog"))
    };
    console.log("State = ",state);
    return state;
  }
}
