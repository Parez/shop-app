import {Routes} from '@angular/router';
import {HomePageComponent} from './pages/home-page/home-page.component';
import {ItemComponent} from './components/item/item.component';
import {ItemPageComponent} from './pages/item-page/item-page.component';
import {CartPageComponent} from './pages/cart-page/cart-page.component';
import {CheckoutPageComponent} from './pages/checkout-page/checkout-page.component';
/**
 * Created by baunov on 28/05/2017.
 */
export const appRoutes: Routes = [
    { path: '', component: HomePageComponent, pathMatch: 'full' },
    { path: 'item/:id', component: ItemPageComponent },
    { path: 'cart', component: CartPageComponent},
    { path: 'checkout', component: CheckoutPageComponent},
];