import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { ItemComponent } from './components/item/item.component';
import { ItemListComponent } from './components/item-list/item-list.component';
import {ItemsService} from './services/items.service';
import { ContactFormComponent } from './components/contact-form/contact-form.component';
import { CartIconComponent } from './components/cart-icon/cart-icon.component';
import { ItemCountComponent } from './components/item-count/item-count.component';
import { CartPreviewComponent } from './components/cart-preview/cart-preview.component';
import { RowItemListComponent } from './components/row-item-list/row-item-list.component';
import { RowItemComponent } from './components/row-item/row-item.component';
import { CartPreviewItemComponent } from './components/cart-preview-item/cart-preview-item.component';
import { CartPreviewItemListComponent } from './components/cart-preview-item-list/cart-preview-item-list.component';
import { ItemPageComponent } from './pages/item-page/item-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { CartPageComponent } from './pages/cart-page/cart-page.component';
import { HeaderComponent } from './components/header/header.component';
import { BasePageComponent } from './pages/base-page/base-page.component';
import { PageBlockComponent } from './components/page-block/page-block.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { StickyDirective } from './directives/sticky.directive';
import {RouterModule} from '@angular/router';
import {appRoutes} from './routes';
import {VgCoreModule} from 'videogular2/src/core/core';
import {VgControlsModule} from 'videogular2/src/controls/controls';
import {VgOverlayPlayModule} from 'videogular2/src/overlay-play/overlay-play';
import {VgBufferingModule} from 'videogular2/src/buffering/buffering';
import { VideoPlayerComponent } from './components/video-player/video-player.component';

import { InfiniteScrollModule } from 'ngx-infinite-scroll';

import { EffectsModule } from '@ngrx/effects';
import {StoreModule} from '@ngrx/store';
import {reducer} from './redux/reducers/';
import {CatalogEffects} from './redux/effects/catalog-effects';
import {LocalStorageService} from './services/local-storage.service';
import { CheckoutPageComponent } from './pages/checkout-page/checkout-page.component';
import { CheckoutFormComponent } from './components/checkout-form/checkout-form.component';
import { AddItemPageComponent } from './pages/admin/add-item-page/add-item-page.component';
import { AddItemFormComponent } from './components/admin/add-item-form/add-item-form.component';

//console.log("Initial state");

export function savedStateDeclaration() {
  return LocalStorageService.getSavedState();
}

//TODO: refactor to separate modules

@NgModule({
  declarations: [
    AppComponent,
    ItemComponent,
    ItemListComponent,
    ContactFormComponent,
    CartIconComponent,
    ItemCountComponent,
    CartPreviewComponent,
    RowItemListComponent,
    RowItemComponent,
    CartPreviewItemComponent,
    CartPreviewItemListComponent,
    ItemPageComponent,
    HomePageComponent,
    CartPageComponent,
    HeaderComponent,
    BasePageComponent,
    PageBlockComponent,
    NavbarComponent,
    StickyDirective,
    VideoPlayerComponent,
    CheckoutPageComponent,
    CheckoutFormComponent,
    AddItemPageComponent,
    AddItemFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    VgCoreModule,
    VgControlsModule,
    VgOverlayPlayModule,
    VgBufferingModule,
    RouterModule.forRoot(appRoutes),
    StoreModule.provideStore(
        reducer
    ),
    EffectsModule.run(CatalogEffects),
    InfiniteScrollModule,
    ReactiveFormsModule
  ],
  providers: [ItemsService, LocalStorageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
