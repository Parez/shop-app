import {Component, OnInit, Input} from '@angular/core';
import {Item} from '../../models/Item';
import {Observable} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {ItemsService} from '../../services/items.service';

@Component({
  selector: 'app-item-page',
  templateUrl: './item-page.component.html',
  styleUrls: ['./item-page.component.scss']
})
export class ItemPageComponent implements OnInit {

  @Input() item$:Observable<Item>;

  constructor(private route: ActivatedRoute, private itemsService:ItemsService) { }

  ngOnInit() {

    this.item$ = this.route.params.do(console.log).switchMap( (params) => {
      let uid = params["id"];
      console.log(uid);
      return this.itemsService.getItemById(uid);
    }).do(console.log);

    this.item$.subscribe();
  }

}
