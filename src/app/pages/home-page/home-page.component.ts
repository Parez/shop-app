import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {ItemsService} from '../../services/items.service';
import {Item} from '../../models/Item';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  items$:Observable<Item[]>;

  pageNum:number = 1;
  pageSize:number = 10;

  constructor(private itemsService:ItemsService) { }

  ngOnInit() {
    this.items$ = this.itemsService.loadedItems$;
    this.itemsService.loadNext(10);
  }

  onScroll()
  {
    console.log("SCROLL");
    this.itemsService.loadNext(10);
  }

}
