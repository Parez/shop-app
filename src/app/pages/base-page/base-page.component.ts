import {Component, OnInit, Input, Inject, ViewChild, ElementRef} from '@angular/core';
import { HostListener} from "@angular/core";
import {DOCUMENT} from '@angular/platform-browser';

@Component({
  selector: 'app-base-page',
  templateUrl: './base-page.component.html',
  styleUrls: ['./base-page.component.scss']
})

export class BasePageComponent implements OnInit {

  @ViewChild("header") header:ElementRef;
  @ViewChild("content") content:ElementRef;

  videoUrl:string = "";
  imageUrl:string = "";

  @Input() headerHeight:string = "100vh";
  @Input() fullWidth:boolean = true;
  @Input() set headerSrc(val:string)
  {
    if(val.indexOf(".mp4") > - 1 ||
        val.indexOf(".flv") > -1 ||
        val.indexOf(".mov") > -1)
    {
      this.videoUrl = val;
    }
    else {
      this.imageUrl = val;
    }
  }


  constructor(@Inject(DOCUMENT) private document: Document) { }

  ngOnInit() {
    console.log();
    console.log(this.content.nativeElement);
  }

  @HostListener('window:scroll', ['$event'])
  onScroll(event) {
    let scroll = window.pageYOffset;
    this.header.nativeElement.style.transform = `translate3d(0px, ${scroll * 0.3}px, 0px)`;
    //console.log();
  }
}
